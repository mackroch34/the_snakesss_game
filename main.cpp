#include "code.cpp"

int main() {
  Snake s1(2, 0);
  Snake s2(2, 3);

  s1.Cells[0].x = 0;
  s1.Cells[0].y = 0;

  s2.Cells[0].x = width - 1;
  s2.Cells[0].y = height - 1;

  bool game = true;
  srand(time(0));

  sf::RenderWindow window(sf::VideoMode(ts * (width + 1), ts * (height + 1)),
                          "SnakessS!");

  sf::Texture t;
  t.loadFromFile("textures/tiles.png");
  sf::Sprite tiles(t);

  sf::Texture sn1;
  sn1.loadFromFile("textures/snake1.png");
  sf::Sprite snake1(sn1);

  sf::Texture sn2;
  sn2.loadFromFile("textures/snake2.png");
  sf::Sprite snake2(sn2);

  sf::Texture ap;
  ap.loadFromFile("textures/apple.png");
  sf::Sprite apple(ap);

  sf::Texture go;
  go.loadFromFile("textures/gameover.png");
  sf::Sprite gameover(go);
  gameover.setPosition(0, 175);

  sf::Clock clock;
  float timer = 0;
  float delay = 0.1;

  for (int i = 0; i < numFruits; ++i) {
    fruits[i].x = rand() % width;
    fruits[i].y = rand() % height;
  }

  while (window.isOpen()) {
    if (!game && sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
      window.clear();

      s1.length = 2;
      s2.length = 2;

      s1.direction = 0;
      s2.direction = 3;

      s1.Cells[0].x = 0;
      s1.Cells[0].y = 0;

      s2.Cells[0].x = width - 1;
      s2.Cells[0].y = height - 1;
      game = true;
    }

    float time = clock.getElapsedTime().asSeconds();
    clock.restart();
    timer += time;

    sf::Event event;

    while (window.pollEvent(event)) {
      if (event.type == sf::Event::Closed) {
        window.close();
      }
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
      if (s2.direction != 2) {
        s2.direction = 1;
      }
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
      if (s2.direction != 1) {
        s2.direction = 2;
      }
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
      if (s2.direction != 0) {
        s2.direction = 3;
      }
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
      if (s2.direction != 3) {
        s2.direction = 0;
      }
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
      if (s1.direction != 2) {
        s1.direction = 1;
      }
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
      if (s1.direction != 1) {
        s1.direction = 2;
      }
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) {
      if (s1.direction != 0) {
        s1.direction = 3;
      }
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
      if (s1.direction != 3) {
        s1.direction = 0;
      }
    }

    if (timer > delay && game) {
      timer = 0;
      UpdateWindow(game, s1, s2);
    }

    window.clear();

    DrawTiles(tiles, window);

    DrawSnake(s1, snake1, game, window);
    DrawSnake(s2, snake2, game, window);

    DrawApples(apple, window);

    if (!game) {
      window.draw(gameover);
    }

    window.display();
  }
  return 0;
}