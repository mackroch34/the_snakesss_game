#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <ctime>

const int width = 30;
const int height = 20;
const int ts = 25;
const int numFruits = 10;

struct Cell {
  int x;
  int y;
} fruits[numFruits];

struct Snake {
  int length;
  int direction;
  Cell Cells[600];
  Snake() = default;
  Snake(int len, int dir) : length(len), direction(dir) {}
};

void DrawTiles(sf::Sprite& tiles, sf::RenderWindow& window) {
  for (int i = 0; i <= width; ++i) {
    for (int j = 0; j <= height; ++j) {
      tiles.setPosition(i * ts, j * ts);
      window.draw(tiles);
    }
  }
}

void DrawSnake(Snake& s, sf::Sprite& snake, bool& game,
               sf::RenderWindow& window) {
  for (int i = 0; i < s.length; ++i) {
    if (i != 0) {
      snake.setTextureRect(sf::IntRect(0, 0, ts, ts));
    } else {
      snake.setTextureRect(sf::IntRect(s.direction * ts, ts, ts, ts));
    }
    if (!game && i == 1) {
      snake.setTextureRect(sf::IntRect(s.direction * ts, ts * 2, ts, ts));
    }
    snake.setPosition(s.Cells[i].x * ts, s.Cells[i].y * ts);

    window.draw(snake);
  }
}

void DrawApples(sf::Sprite& apple, sf::RenderWindow& window) {
  for (int i = 0; i < numFruits; ++i) {
    apple.setPosition(fruits[i].x * ts, fruits[i].y * ts);
    window.draw(apple);
  }
}

void ControlSnake(Snake& s, bool& game) {
  for (int i = s.length; i > 0; i--) {
    s.Cells[i].x = s.Cells[i - 1].x;
    s.Cells[i].y = s.Cells[i - 1].y;
  }

  if (s.direction == 0) {
    s.Cells[0].y += 1;
  }
  if (s.direction == 1) {
    s.Cells[0].x -= 1;
  }
  if (s.direction == 2) {
    s.Cells[0].x += 1;
  }
  if (s.direction == 3) {
    s.Cells[0].y -= 1;
  }

  if (s.Cells[0].x > width) {
    s.Cells[0].x = 0;
  }
  if (s.Cells[0].x < 0) {
    s.Cells[0].x = width;
  }
  if (s.Cells[0].y > height) {
    s.Cells[0].y = 0;
  }
  if (s.Cells[0].y < 0) {
    s.Cells[0].y = height;
  }

  // for (int i = 1; i < s.length; ++i) {
  //   if ((s.Cells[0].x == s.Cells[i].x) && (s.Cells[0].y == s.Cells[i].y)) {
  //     game = false;
  //   }
  // }

  for (int i = 0; i < s.length; ++i) {
    for (int j = 0; j < numFruits; ++j) {
      if ((s.Cells[i].x == fruits[j].x) && (s.Cells[i].y == fruits[j].y)) {
        fruits[j].x = rand() % width;
        fruits[j].y = rand() % height;
        ++s.length;
      }
    }
  }
}

void UpdateWindow(bool& game, Snake& s1, Snake& s2) {
  ControlSnake(s1, game);
  ControlSnake(s2, game);
  for (int i = 0; i < s1.length; ++i) {
    for (int j = 0; j < s2.length; ++j) {
      if ((s1.Cells[i].x == s2.Cells[j].x) &&
          (s1.Cells[i].y == s2.Cells[j].y)) {
        game = false;
      }
    }
  }
}